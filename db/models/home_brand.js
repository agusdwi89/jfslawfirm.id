'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class home_brand extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  home_brand.init({
    image: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'home_brand',
  });
  return home_brand;
};