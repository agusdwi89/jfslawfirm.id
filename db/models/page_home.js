'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class page_home extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  page_home.init({
    jargon: DataTypes.STRING,
    welcome_t1: DataTypes.STRING,
    welcome_t2: DataTypes.STRING,
    welcome_text: DataTypes.TEXT,
    welcome_image: DataTypes.STRING,
    welcome_button: DataTypes.STRING,
    welcome_button_link: DataTypes.STRING,
    practice_t1: DataTypes.STRING,
    practice_t2: DataTypes.STRING,
    practice_description: DataTypes.STRING,
    practice_button: DataTypes.STRING,
    practice_button_link: DataTypes.STRING,
    proccess_title: DataTypes.STRING,
    proccess_description: DataTypes.STRING,
    proccess_detail: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'page_home',
  });
  return page_home;
};