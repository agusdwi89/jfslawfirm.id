'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('page_homes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      jargon: {
        type: Sequelize.STRING
      },
      welcome_t1: {
        type: Sequelize.STRING
      },
      welcome_t2: {
        type: Sequelize.STRING
      },
      welcome_text: {
        type: Sequelize.TEXT
      },
      welcome_image: {
        type: Sequelize.STRING
      },
      welcome_button: {
        type: Sequelize.STRING
      },
      welcome_button_link: {
        type: Sequelize.STRING
      },
      practice_t1: {
        type: Sequelize.STRING
      },
      practice_t2: {
        type: Sequelize.STRING
      },
      practice_description: {
        type: Sequelize.STRING
      },
      practice_button: {
        type: Sequelize.STRING
      },
      practice_button_link: {
        type: Sequelize.STRING
      },
      proccess_title: {
        type: Sequelize.STRING
      },
      proccess_description: {
        type: Sequelize.STRING
      },
      proccess_detail: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('page_homes');
  }
};