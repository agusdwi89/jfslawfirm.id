const { slider, page_home, home_brand } = require("../../db/models/index")

class HomeController {

    constructor() { }

    async index(req, res, next) {

        let brands = await home_brand.findAll()

        var data = {
            'sliders': await slider.findAll(),
            'page': await page_home.findAll(),
            'brands': brands,
            'brands_count': (brands.length < 6) ? brands.length : 6 ,
            'practice': [1, 2, 3, 4],
            'proccess': {
                title: 'A UNIQUE PROCESS BASED ON CREATING IMPACT',
                subTitle: 'Our expert attorneys provide the highest legal service to protect YOUR rights and secure the BEST possible outcome for your case!! Our team has received dismissals, not guilty verdicts, or no-point resolutions for 85-90% of the clients.',
                item: [1, 2, 3, 4, 5, 6]
            }
        };

        res.render('pages/home', { layout: 'index', data });
    }

}

module.exports = new HomeController().index;