const express = require('express');
const app = express();
const port = 3111;
const routes = require('./src/http/route/route');
const handlebars = require('express-handlebars');
const path = require('path');
const env = require('dotenv');

console.log(__dirname + '/src/views/layouts')

app.use(express.static('public'))
app.set('view engine', 'hbs');
app.set('views', path.join("src", "views"));
app.engine('hbs', handlebars({
    layoutsDir: __dirname + '/src/views/layouts',
    extname: 'hbs',
    defaultLayout: 'planB',
    partialsDir: __dirname + '/src/views/partials/'
}));

app.use('/', routes);

app.listen(port, () => console.log(`App listening to port ${port}`))